#include<iostream>
using namespace std;
class Functor
{
public:
    int operator()(int a, int b)
    {
	cout<<"I am functor"<<endl;
        return a < b;
    }
};

int main()
{
    Functor f;
    int a = 8;
    int b = 7;
    int ans = f(a, b);
}

